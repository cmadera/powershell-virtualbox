
param(
    [string]$action,
    [String]$vmToClone = "Alpine",
    [String]$numOfClones = 5
)

function listVMs {
    # Get the list of running VMs
    $runningVmList = & VBoxManage list vms
    $vmNames = $runningVmList | ForEach-Object { $_ -split ' ' | Select-Object -First 1 }

    # Iterate through each VM and display its name and status
    foreach ($vm in $vmNames) {
        $vmName = $vm -replace '^"(.+)"$', '$1' 
        $vmStatus = & VBoxManage showvminfo $vmName --machinereadable | Select-String 'VMState=' | ForEach-Object { $_ -replace '^VMState="(.+)"$', '$1' }

        Write-Host "VM Name: $vmName  Status: $vmStatus"
    }
}

function cloneVMs {
    param (
        [string]$cloneName,
        [string]$numOfClones
    )
    Write-Host "Clone $numOfClones $cloneName"
    # Loop through and create clones
    for ($i = 1; $i -le $numOfClones; $i++) {
        $cloneName = "${vmToClone}Clone$i"
        $command = "VBoxManage clonevm $vmToClone --name=$cloneName --register"
        Invoke-Expression $command
    }
}

function startVMs {
        # Get the list of running VMs
    $runningVmList = & VBoxManage list vms

    # Extract VM names from the list
    $vmNames = $runningVmList | ForEach-Object { $_ -split ' ' | Select-Object -First 1 }

    # Start each VM
    foreach ($vmName in $vmNames) {
        if ($vmName -like "*Clone*") {
            Write-Host "Starting VM: $vmName"
            $command = "VBoxManage startvm $vmName --type headless"
            Invoke-Expression $command
        }
    }

    Write-Host "All VMs started successfully!"
    
}

function stopVMs {
    # Get the list of running VMs
    $runningVmList = & VBoxManage list runningvms

    # Extract VM names from the list
    $vmNames = $runningVmList | ForEach-Object { $_ -split ' ' | Select-Object -First 1 }

    # Start each VM
    foreach ($vmName in $vmNames) {
        Write-Host "Poweroff VM: $vmName"
        $command = "VBoxManage controlvm $vmName poweroff"
        Invoke-Expression $command
    }

    Write-Host "All VMs stopped successfully!"
    
}

function removeVMs {
    # Get the list of running VMs
    $runningVmList = & VBoxManage list vms

    # Stop all running VMs
    stopVMs

    # Extract VM names from the list
    $vmNames = $runningVmList | ForEach-Object { $_ -split ' ' | Select-Object -First 1 }

    # Start each VM
    foreach ($vmName in $vmNames) {
        if ($vmName -like "*Clone*") {
            Write-Host "Removing VM: $vmName"
            $command = "VBoxManage unregistervm $vmName -delete"
            Write-Host $command
            Invoke-Expression $command
        }
    }

    Write-Host "All VMs removed successfully!"
}

function ShowHelp {
    Write-Host "Available actions:"
    Write-Host "  list: List of VMs"
    Write-Host "  clone: Clone X VMsNames"
    Write-Host "  start: Start all Cloned VMs."
    Write-Host "  stop: Stop all running Cloned VMs."
    Write-Host "  remove: Remove all Cloned VMs."
}

function setup {
    # PowerShell script to verify VirtualBox path in PATH environment variable

    # Define the default VirtualBox installation path
    $defaultVirtualBoxPath = "C:\Program Files\Oracle\VirtualBox"

    # Check if VirtualBox is already in the PATH
    if ($env:Path -like "*VirtualBox*") {
        Write-Host "VirtualBox is already in the PATH."
    } else {
        # Add the default VirtualBox path to the PATH environment variable
        $env:Path += ";$defaultVirtualBoxPath"
        Write-Host "Added VirtualBox path to the PATH environment variable."
    }

    # Verify the updated PATH
    #Write-Host "Updated PATH environment variable:"
    #$env:Path
}

setup
#$env:PATH = $env:PATH + ";C:\Program Files\Oracle\VirtualBox"

switch ($action) {
    "list"  { listVMs }
    "clone" { cloneVMs -cloneName $cloneName -numOfClones $numOfClones }
    "start" { startVMs }
    "stop"  { stopVMs }
    "remove" { removeVMs }
    "help"  { ShowHelp }
    default { ShowHelp }
}