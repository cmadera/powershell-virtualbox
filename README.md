# VirtualBox VM Management Script

This PowerShell script allows you to manage VirtualBox VMs by listing, cloning, starting, stopping and removing them. It's useful for automating common tasks related to VirtualBox.

## Usage

Make sure you have VirtualBox installed on your system and open a PowerShell terminal to run.

1. **Clone VMs:**
   - To clone a specified VM (default: "Alpine"), use the following command:
     ```
     .\vms.ps1 -action clone -vmToClone "Alpine" -numOfClones 5
     ```
   - This will create 5 clones of the "Alpine" VM (named "AlpineClone1" to "AlpineClone5").

2. **To list all VMs, execute:**
     ```
     .\vms.ps1 list
     ```

3. **To start all the Cloned VMs ("with `Clone` name"), run:**
     ```
     .\vms.ps1 start
     ```

4. **To stop all the running Cloned VMs ("with `Clone` name"), run:**
     ```
     .\vms.ps1 stop
     ```

5. **To remove all the Cloned VMs ("with `Clone` name"), run:**
   
     ```
     .\vms.ps1 remove
     ```

6. **Help:**
     ```
     .\vms.ps1 help
     ```

## Prerequisites

- Ensure that VirtualBox is installed and its executable (`VBoxManage.exe`) is in your system's PATH.
- The default path for VirtualBox is: "C:\Program Files\Oracle\VirtualBox"

## Notes

- Adjust the `$vmToClone` variable to specify the VM you want to clone.
- Modify the `$numOfClones` variable to control the number of clones to create.

